``crx_unpack`` Package
======================

.. automodule:: crx_unpack
    :members: unpack, verify_signature, extract_zip, BadCrxHeader
    :show-inheritance:
