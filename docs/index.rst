CRX Unpack Utilities
====================

.. toctree::
    :maxdepth: 2
    :hidden:

    Home <self>
    crx_unpack
    encrypted_temp

.. include:: ../README.rst
    :start-after: main_intro
    :end-before: end_main_intro

The first module is :doc:`crx_unpack <crx_unpack>`, which handles the headers and structure of the CRX itself (see
below for more details on this).

The second module is :doc:`encrypted_temp`, which gives a way to use `eCryptfs <http://ecryptfs.org/>`_ to encrypt a
directory that only ever exists in memory by inheriting from the :py:func:`~tempfile.TemporaryDirectory` class and
hooking into some eCryptfs tools to handle the encryption of the file contents and file names (handled by different
keys).


.. include:: ../README.rst
    :start-after: begin_import

